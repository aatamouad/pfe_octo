package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;

public class UtilisateurMapper {



    private static UtilisateurDto utilisateurDto;
    private static Utilisateur utilisateur;

    // From Utilisateur To Utilisateur DTO
    public static UtilisateurDto entityToDto(Utilisateur utilisateur) {
        utilisateurDto = UtilisateurDto.builder()
                .birthdate(utilisateur.getBirthdate())
                .firstname(utilisateur.getFirstname())
                .gender(utilisateur.getGender())
                .lastname(utilisateur.getLastname())
                .username(utilisateur.getUsername())
                .build();
        return utilisateurDto;
    }

    // From Utilisateur DTO To Utilisateur
    public static Utilisateur dtoToEntity(UtilisateurDto utilisateurDto){
        utilisateur = Utilisateur.builder()
                .birthdate(utilisateurDto.getBirthdate())
                .firstname(utilisateurDto.getFirstname())
                .gender(utilisateurDto.getGender())
                .lastname(utilisateurDto.getLastname())
                .username(utilisateurDto.getUsername())
                .build();
        return utilisateur;
    }
}

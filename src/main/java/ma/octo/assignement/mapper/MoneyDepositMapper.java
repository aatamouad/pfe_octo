package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.MoneyDepositDto;

import java.util.List;
import java.util.stream.Collectors;

public class MoneyDepositMapper {








    private static MoneyDepositDto moneyDepositDto;
    private static MoneyDeposit moneyDeposit;

    // From MoneyDeposit To MoneyDeposit Dto
    public static MoneyDepositDto entityToDto(MoneyDeposit moneyDeposit) {
        moneyDepositDto = MoneyDepositDto.builder()
                .motifDeposit(moneyDeposit.getMotifDeposit())
                .nrCompteBeneficiaire(moneyDeposit.getCompteBeneficiaire().getNrCompte())
                .dateExecution(moneyDeposit.getDateExecution())
                .Montant(moneyDeposit.getMontant())
                .nomPrenomEmetteur(moneyDeposit.getNomPrenomEmetteur())
                .build();
        return moneyDepositDto;
    }

    // From MoneyDeposit DTO To MoneyDeposit
    public static MoneyDeposit dtoToEntity(MoneyDepositDto moneyDepositDto){
        Compte compteBeneficiaire = Compte.builder().nrCompte(moneyDepositDto.getNrCompteBeneficiaire()).build();

        moneyDeposit = MoneyDeposit.builder()
                .motifDeposit(moneyDepositDto.getMotifDeposit())
                .compteBeneficiaire(compteBeneficiaire)
                .dateExecution(moneyDepositDto.getDateExecution())
                .Montant(moneyDepositDto.getMontant())
                .nomPrenomEmetteur(moneyDepositDto.getNomPrenomEmetteur())
                .build();
        return moneyDeposit;
    }

    public static List<MoneyDepositDto> mapToListDto(List<MoneyDeposit> moneyDeposits) {
        return moneyDeposits
                .stream()
                .map(MoneyDepositMapper::entityToDto)
                .collect(Collectors.toList());
    }
}

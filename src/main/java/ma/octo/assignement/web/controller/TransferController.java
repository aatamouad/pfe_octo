package ma.octo.assignement.web.controller;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.exceptions.TransferNonExistantException;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.TransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController(value = "/transfers")
class TransferController {

    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(TransferController.class);

    @Autowired
    private TransferService transferService;


    private final UtilisateurRepository re3;

    @Autowired
    TransferController(UtilisateurRepository re3) {
        this.re3 = re3;
    }

    @GetMapping("listDesTransferts")
    List<TransferDto> loadAll() throws TransferNonExistantException {
        return transferService.loadAll();
    }




    @PostMapping("/executerTransfers")
    @ResponseStatus(HttpStatus.CREATED)
    public Transfer createTransaction(@RequestBody TransferDto transferDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        return transferService.createTransaction(transferDto);
    }

    @GetMapping("/{id}")
    public Transfer getTransfer(@PathVariable Long id) throws TransferNonExistantException {
        return transferService.getTransfer(id);
    }
}

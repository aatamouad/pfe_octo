package ma.octo.assignement.service;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.MoneyDepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.MoneyDepositNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.MoneyDepositMapper;
import ma.octo.assignement.repository.MoneyDepositRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class MoneyDepositService implements IMoneyDepositService{


    public static final BigDecimal MONTANT_MAXIMAL = BigDecimal.valueOf(10000);
    public static final BigDecimal MONTANT_MINIMAL =BigDecimal.TEN;


    @Autowired
    private  MoneyDepositRepository moneyDepositRepository;
    @Autowired
    private  IAuditService auditService;
    @Autowired
    private  ICompteService compteService;






    @Override
    public List<MoneyDepositDto> loadAll() throws MoneyDepositNonExistantException {
        List<MoneyDeposit> all = moneyDepositRepository.findAll();
        List<MoneyDepositDto> response = all.stream().map(MoneyDepositMapper::entityToDto).collect(Collectors.toList());
        if (all.isEmpty()) {
            throw new MoneyDepositNonExistantException("Aucun deposit trouvé");
        } else {
            return response;
        }
    }

    @Override
    public MoneyDepositDto createTransaction(MoneyDepositDto MoneyDepositDto) throws TransactionException, CompteNonExistantException {
        Compte compteBeneficiaire = compteService.getCompteByNrCompte(MoneyDepositDto.getNrCompteBeneficiaire());

        if (MoneyDepositDto.getMontant() == null || MoneyDepositDto.getMontant().doubleValue() == 0) {
            log.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (MoneyDepositDto.getMontant().compareTo(MONTANT_MINIMAL) < 0 ) {
            log.error("Montant minimal de MoneyDeposit non atteint");
            throw new TransactionException("Montant minimal de MoneyDeposit non atteint");
        } else if (MoneyDepositDto.getMontant().compareTo(MONTANT_MAXIMAL) > 0 ) {
            log.error("Montant maximal de MoneyDeposit dépassé");
            throw new TransactionException("Montant maximal de MoneyDeposit dépassé");
        }

        if (MoneyDepositDto.getMotifDeposit().length() == 0 ) {
            log.error("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (MoneyDepositDto.getNomPrenomEmetteur() == null || MoneyDepositDto.getNomPrenomEmetteur().isEmpty()) {
            log.error("Le nom et le prenom de l'emetteur est vide");
            throw new TransactionException("Le nom et le prenom de l'emetteur est vide");
        }

        compteBeneficiaire.setSolde(BigDecimal.valueOf(compteBeneficiaire.getSolde().doubleValue() + MoneyDepositDto.getMontant().doubleValue()));

        MoneyDeposit MoneyDeposit = MoneyDepositMapper.dtoToEntity(MoneyDepositDto);
        MoneyDeposit.setCompteBeneficiaire(compteBeneficiaire);
        MoneyDeposit.setDateExecution(new Date());

        moneyDepositRepository.save(MoneyDeposit);

        auditService.addAudit("MoneyDeposit depuis " + MoneyDeposit.getNomPrenomEmetteur() + " vers " + MoneyDeposit
                .getCompteBeneficiaire() + " d'un montant de " + MoneyDeposit.getMontant()
                .toString(), EventType.DEPOSIT);

        return MoneyDepositMapper.entityToDto(moneyDepositRepository.save(MoneyDeposit));
    }

    @Override
    public MoneyDeposit getMoneyDeposit(Long id) throws MoneyDepositNonExistantException {
        return moneyDepositRepository.findById(id).orElseThrow(() -> new MoneyDepositNonExistantException("Le Deposit n'existe pas"));
    }
}

package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;

import java.util.List;

public interface ICompteService {


    Compte getCompteByNrCompte(String nrCompte) throws CompteNonExistantException;
    Compte AddCompte(Compte compte) ;
    List<CompteDto> loadAllCompte() throws CompteNonExistantException;
}

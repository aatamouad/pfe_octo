package ma.octo.assignement.service;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AuditService implements IAuditService {


    Logger LOGGER = LoggerFactory.getLogger(AuditService.class);

    @Autowired
    private AuditRepository auditRepository;


    @Override
    public Audit addAudit(String message, EventType auditType) {

        LOGGER.info("Audit de l'événement {}", auditType);
        LOGGER.info(message);
        Audit audit =new Audit();
        audit.setEventType(auditType);
        audit.setMessage(message);
        return auditRepository.save(audit);
    }
}

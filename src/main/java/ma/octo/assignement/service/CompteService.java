package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
@Service
public class CompteService implements ICompteService{

    @Autowired
    private  CompteRepository compteRepository;


    @Override
    public Compte getCompteByNrCompte(String nrCompte) throws CompteNonExistantException {
        Compte compte = compteRepository.findByNrCompte(nrCompte);
        if (compte == null) {
            throw new CompteNonExistantException();
        }
        return compte;
    }

    @Override
    public Compte AddCompte(Compte compte)  {
        return compteRepository.save(compte);
    }

    @Override
    public List<CompteDto> loadAllCompte() throws CompteNonExistantException {
        List<Compte> allcomptes = compteRepository.findAll();
        List<CompteDto> response = allcomptes.stream().map(CompteMapper::entityToDto).collect(Collectors.toList());
        if (allcomptes.isEmpty()) {
            throw new CompteNonExistantException();
        } else {
            return response;
        }



    }
}

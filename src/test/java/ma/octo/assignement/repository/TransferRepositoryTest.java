package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.MoneyDepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.MoneyDepositNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.MoneyDepositMapper;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.MoneyDepositService;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
@ExtendWith(MockitoExtension.class)
public class TransferRepositoryTest {

  @Mock
  private MoneyDepositRepository moneyDepositRepository;

  @Mock
  private CompteService compteService;



  @InjectMocks
  private MoneyDepositService moneyDepositService;

  private final Date now = new Date();

  @Test
  void canGetMoneyDeposit() {
    // given
    Long id = 1L;
    MoneyDepositDto moneyDepositDto = MoneyDepositDto.builder()
            .nomPrenomEmetteur("Mouad aatafay")
            .Montant(BigDecimal.TEN)
            .motifDeposit("Motif")
            .dateExecution(now)
            .nrCompteBeneficiaire("Nr1").build();

    MoneyDeposit moneyDeposit = MoneyDeposit.builder()
            .compteBeneficiaire(new Compte())
            .motifDeposit("Motif")
            .nomPrenomEmetteur("Mouad aatafay")
            .dateExecution(now)
            .Montant(BigDecimal.TEN).build();

    try(MockedStatic<MoneyDepositMapper> mockStatic = Mockito.mockStatic(MoneyDepositMapper.class)) {
      //when
      mockStatic.when((MockedStatic.Verification) MoneyDepositMapper.entityToDto(moneyDeposit)).thenReturn(moneyDepositDto);
      when(moneyDepositRepository.findById(id)).thenReturn(Optional.of(moneyDeposit));

      MoneyDepositDto result = MoneyDepositMapper.entityToDto(moneyDepositService.getMoneyDeposit(id));

      //then
      assertEquals("Mouad aatafay", result.getNomPrenomEmetteur());
      assertEquals(BigDecimal.TEN, result.getMontant());
      assertEquals("Motif", result.getMotifDeposit());
      assertEquals("Nr1", result.getNrCompteBeneficiaire());
      assertEquals(now, result.getDateExecution());

    } catch (MoneyDepositNonExistantException e) {
      e.printStackTrace();
    }
  }

  @Test
  void getMoneyDepositWillThrowExceptionWhenIdNotFound() {
    // given
    Long id = 1L;
    MoneyDepositDto moneyDepositDto = MoneyDepositDto.builder()
            .nomPrenomEmetteur("Mouad aatafay")
            .Montant(BigDecimal.TEN)
            .motifDeposit("Motif")
            .dateExecution(now)
            .nrCompteBeneficiaire("Nr1").build();

    MoneyDeposit moneyDeposit = MoneyDeposit.builder()
            .compteBeneficiaire(new Compte())
            .motifDeposit("Motif")
            .nomPrenomEmetteur("Mouad aatafay")
            .dateExecution(now)
            .Montant(BigDecimal.TEN).build();

    try(MockedStatic<MoneyDepositMapper> mockStatic = Mockito.mockStatic(MoneyDepositMapper.class)) {
      //when
      mockStatic.when((MockedStatic.Verification) MoneyDepositMapper.entityToDto(moneyDeposit)).thenReturn(moneyDepositDto);
      when(moneyDepositRepository.findById(id)).thenReturn(Optional.empty());

      //then
      assertThatThrownBy( () -> moneyDepositService.getMoneyDeposit(id))
              .isInstanceOf(MoneyDepositNonExistantException.class);
    }
  }



  @Test
  void createTransactionWillThrowWhenMontantIsNull(){
    // given
    MoneyDepositDto moneyDepositDto = MoneyDepositDto.builder()
            .nomPrenomEmetteur("Mouad aatafay")
            .Montant(BigDecimal.ZERO)
            .motifDeposit("Motif")
            .dateExecution(now)
            .nrCompteBeneficiaire("Nr1").build();

    // when
    // then
    assertThatThrownBy( () -> moneyDepositService.createTransaction(moneyDepositDto))
            .isInstanceOf(TransactionException.class)
            .hasMessageContaining("Montant vide");
  }

  @Test
  void createTransactionWillThrowWhenMontantIsGreaterThanMontantMax() {
    // given
    MoneyDepositDto moneyDepositDto = MoneyDepositDto.builder()
            .nomPrenomEmetteur("Mouad aatafay")
            .Montant(new BigDecimal("100001"))
            .motifDeposit("Motif")
            .dateExecution(now)
            .nrCompteBeneficiaire("Nr1").build();
    // when
    // then
    assertThatThrownBy( () -> moneyDepositService.createTransaction(moneyDepositDto))
            .isInstanceOf(TransactionException.class)
            .hasMessageContaining("Montant maximal de MoneyDeposit dépassé");
  }

  @Test
  void createTransactionWillThrowWhenMontantIsLessThanMontantMin() {
    // given
    MoneyDepositDto moneyDepositDto = MoneyDepositDto.builder()
            .nomPrenomEmetteur("Mouad aatafay")
            .Montant(new BigDecimal("9"))
            .motifDeposit("Motif")
            .dateExecution(now)
            .nrCompteBeneficiaire("Nr1").build();

    // when
    // then
    assertThatThrownBy( () -> moneyDepositService.createTransaction(moneyDepositDto))
            .isInstanceOf(TransactionException.class)
            .hasMessageContaining("Montant minimal de MoneyDeposit non atteint");
  }




}